package server

import (
	"net"
	"log"
	"google.golang.org/grpc"
	"bitbucket.org/ajitem_s/grpcGo/calculatorpb"
)

func Start(port string) {
	listener, err := net.Listen("tcp", ":"+port)
	if err != nil {
		log.Fatal(err)
	}

	s := grpc.NewServer()
	calculatorpb.RegisterCalculatorServiceServer(s, &CalculatorHandler{})

	if err := s.Serve(listener); err != nil {
		log.Fatal(err)
	}
}
