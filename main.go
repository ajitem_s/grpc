package main

import (
	"bitbucket.org/ajitem_s/grpcGo/server"
	"fmt"
	"bitbucket.org/ajitem_s/grpcGo/client"
)

func main() {
	go func() {
		fmt.Println("Starting Server")
		server.Start("50051")

	}()
	fmt.Println("Starting Client")
	client.Start("50051")
}
